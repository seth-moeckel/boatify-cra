import React, { useState, useEffect } from 'react';
import { Stage, Layer, Text } from 'react-konva';
import './App.css';

import reactLogo from './logo.svg';
import boatHead from './boathead.png';

function App() {
  const [boatified, setBoatified] = useState(false);
  const [boats, setBoatList] = useState([]);
  const [boatCount, setBoatCount] = useState(0);

  return (
    <div className="App"
      onClick={(e) => {
        if (boatified) {
          const newBoat = {
            x: e.nativeEvent.offsetX-15,
            y: e.nativeEvent.offsetY-15,
          };
          setBoatList([newBoat, ...boats]);
          setBoatCount(boatCount+1);
        }
      }}>

      {
        boatified
          ? <Stage className="canvas" width={window.innerWidth} height={window.innerHeight}>
            <Layer>
              {boats.map((boat) =>
                <Text
                  key="boat.x+boat.y"
                  fontSize={30}
                  text="⛵"
                  x={boat.x}
                  y={boat.y}
                />
              )}
            </Layer>
          </Stage>
          : ''
      }

      <button className="boatifier" onClick={() => setBoatified(!boatified)}>
        { boatified ? 'Un-Boatify ⛵' : 'Boatify ⛵' }
      </button>

      <div className="App-header">
        { boatified ? <p className="boat-counter">{boatCount}</p> : '' }
        <img src={boatified ? boatHead : reactLogo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        {
          boatified
            ? <p>Click to add another ⛵</p>
            : <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
        }
      </div>
    </div>
  );
}

export default App;
