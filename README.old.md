# React Hooks

Why you should consider React Hooks:
- Completely opt-in
- Completely backwards-compatible
- Simplifies stateful logic and it's reuse between components

```js
// State hook
const [val, setVal] = useState(initVal);

setVal(1);

// Effect hook
// runs after changes are written to dom
useEffect(() => {
	// do something
	// great way to track when the state changes
	// could be used for subscription

	return () => {
		// do cleanup
		// optional!
	};
});

// context hook
const locale = useContext(LocaleContext);


// reducer hook
const [todos, dispatch] = useReducer(todosReducer);
```

Rules:
- Only call hooks at top level (no loops, nested funcs or conditions)
- Only call hooks from React function components

